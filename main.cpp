#include <iostream>

using namespace std;

class Number{
    int dim;
    int numOfDigits;
    int* digits;


public:
    Number(int _dim,int _numOfDigits){
        dim = _dim;
        numOfDigits = _numOfDigits;
        digits = (int*)malloc(numOfDigits*sizeof(int));
        for (int i = 0; i < numOfDigits; ++i) {
            digits[i] = 0;
        }
    }
    int* getDigits(){
        return digits;
    }
    bool nextNum(){
        // carry handler
        for (int i = 0; i < numOfDigits; ++i) {
            if(digits[i] < dim-1){
                digits[i]++;
                for (int j = 0; j < i; ++j) {
                    digits[j] = 0;
                }
                return true;
            }
        }
        return false;
    }
    void print(){
        for (int i = numOfDigits-1; i >= 0; i--) {
            cout << digits[i];
        }
        cout << endl;
    }
};

/*
 * @param      n := graph size
 * @param      s := subset of {1...n-1}
 * @param s_size := size of s
 * @return incidence matrix
 */
int** generateGraph(int n, const int* s, int s_size){

    int **graph = (int **)malloc(n * sizeof(int*));
    for(int i = 0; i < n; i++) graph[i] = (int *)malloc(n * sizeof(int));

    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            bool found = false;
            for (int k = 0; k < s_size; ++k) {
                if(abs(i-j) == s[k]){
                    graph[i][j] = 1;
                    found = true;
                    break;
                }
            }
            if(!found){
                graph[i][j] = 0;
            }
        }
    }

    return graph;
}

/*
 * @param   graph := incidence matrix
 * @param       n := graph size
 * @param colours := colour of vertex at index
 * @return validity of colouring
 */
bool validColouring(int** graph, const int* colours,int n){
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            if(graph[i][j] == 1 && colours[i] == colours[j]){
                return false;
            }
        }
    }
    return true;
}

/*
 * @param   graph := incidence matrix
 * @param       n := graph size
 * @return chromatic number of the graph
 */
int chromaticNumber(int** graph, int n){

    // Brooks' theorem states that the chromatic number of a graph is at most the maximum vertex degree Delta,
    // unless the graph is complete or an odd cycle, in which case Delta+1 colors are required.
    int maxDeg, neighbourSum = 0;
    for (int i = 0; i < n; ++i){
        neighbourSum = 0;
        for (int j = 0; j < n; ++j){
            if(graph[i][j] == 1){ neighbourSum++; }
        }
        if(neighbourSum > maxDeg){maxDeg = neighbourSum;}
    }


    for (int i = 1; i <= maxDeg; ++i) {
        auto* colorNumber = new Number(i, n);
        do {
            if(validColouring(graph, colorNumber->getDigits(), n)){
                colorNumber->print();
                return i;
            }
        } while (colorNumber->nextNum());
    }

    return maxDeg + 1;
}

int main() {

    int s[4] = {2,3,5,6};
    int n = 7;

    int** graph = generateGraph(n,s,4);

    for (int i = 0; i < n; ++i){
        for (int j = 0; j < n; ++j){
            cout << graph[i][j] << ", ";
        }
        cout << endl;
    }
    cout << endl;

    cout << chromaticNumber(graph, n);

//    auto* n = new Number(2,2);
//
//    for (int i = 0; i < 5; ++i) {
//        cout << "hasNext:" << n->nextNum() << " ";
//        n->print();
//    }

    return 0;
}